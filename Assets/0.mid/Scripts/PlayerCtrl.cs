﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCtrl : MonoBehaviour {
	public ContainerCtrl containerCtrl;
	public GameCtrl_Mid gameCtrl;
	public string nextScene;
	bool collided;
	float delay = 8.0f;
	int idx;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter (Collider col) {
		if (col.tag == "Dead") {
			collided = true;
			SceneManager.LoadScene("Mid_RetryScene");
		} else if (col.tag == "Clear") {
			Debug.Log("CLR");
			CancelInvoke();
			collided = false;
			Invoke("Up", 1.0f);
			Invoke("Next", 2.0f);
		} else if (col.tag == "RedItem") {
			CancelInvoke();
			Invoke("Up", 1.0f);
			containerCtrl.ReExecute(idx, 2);
		} else if (col.tag == "BlueItem") {
			CancelInvoke();
			Invoke("Right", 1.0f);
			Invoke("Up", 2.0f);
			containerCtrl.ReExecute(idx, 3);
		} else if (col.tag == "End") {
			SceneManager.LoadScene(nextScene);
		}
	}

	void OnTriggerStay(Collider col) {
		if (col.tag == "None") {
			if (delay > 0) {
				delay -= Time.deltaTime;
			} else {
				Debug.Log("DEAD");
			}
		}
	}

	public void MoveUp() {
		if (!collided) {
			++idx;
			Up();
		}
	}

	public void MoveLeft() {
		if (!collided) {
			++idx;
			Left();
		}
	}

	public void MoveRight() {
		if (!collided) {
			++idx;
			Right();
		}
	}

	void Up() {
		transform.position += Vector3.forward;
	}

	void Left() {
		transform.position -= Vector3.right;
	}

	void Right() {
		transform.position += Vector3.right;
	}
	void Next() {
		containerCtrl.Flush();
		gameCtrl.Reset();
		idx = 0;
	}
}
