﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameCtrl_03 : MonoBehaviour
{
    public StageCtrl stageCtrlr;
    public FloorCtrl floorCtrlr;
    public GameObject[] players;
    public Text resultText;
    int deadPlayers = 0;
    int deadPlayerNumber = 0;
    // Use this for initialization
    void Start()
    {
        ResetGame();
        floorCtrlr.ResetThis();
    }

    void ResetGame()
    {
        Debug.Log("reset");
        deadPlayerNumber = 0;
        deadPlayers = 0;
        resultText.enabled = false;
        stageCtrlr.ResetThis();
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("ItemPower")) {
            Destroy(g);
        }
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("ItemSpeed")) {
            Destroy(g);
        }
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("ItemLay")) {
            Destroy(g);
        }
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("Bomb")) {
            Destroy(g);
        }
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("Boom")) {
            Destroy(g);
        }
        for (int i = 0; i < players.Length; ++i)
        {
            players[i].GetComponent<BazziCtrl>().ResetBazzi();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayerDead(int playerNumber)
    {
        ++deadPlayers;
        deadPlayerNumber = playerNumber;
        StartCoroutine(CheckDead());
    }

    IEnumerator Retry()
    {
        yield return new WaitForSeconds(2f);
        resultText.text = "ANY KEY TO RESTART\nESC TO EXIT";
        while (true)
        {
            if (Input.GetKey(KeyCode.Escape)) {
				SceneManager.LoadScene("MainScene");
            }
            else if (Input.anyKey)
            {
                ResetGame();
                resultText.enabled = false;
                break;
            }
			yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator CheckDead()
    {
        yield return new WaitForSeconds(0.5f);
        Debug.Log(deadPlayers);
        if (deadPlayers == 1)
        {
            if (deadPlayerNumber == 1)
            {
                resultText.text = "PLAYER 2 WIN!";
                resultText.enabled = true;
            }
            else if (deadPlayerNumber == 2)
            {
                resultText.text = "PLAYER 1 WIN!";
                resultText.enabled = true;
            }
        }
        else
        {
            resultText.text = "Draw";
            resultText.enabled = true;
        }
        StartCoroutine(Retry());
    }
}
