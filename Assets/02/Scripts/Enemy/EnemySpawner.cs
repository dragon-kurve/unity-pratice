﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public List<GameObject> Enemy = new List<GameObject>();
    public Vector2 widthHeight = new Vector2(10, 5);
    public float speed = 1;
    public bool started = false;

    private bool movingRight = true;
    private Vector2 maxMove;

    private float nextShot;

    void Start()
    {
        NextShot();

        maxMove.x = Camera.main.ViewportToWorldPoint(new Vector3(0, 0)).x;
        maxMove.y = Camera.main.ViewportToWorldPoint(new Vector3(1, 0)).x;
    }

    public void SpawnUntilFull()
    {
        Transform next = NextPosition();
        if (next)
        {
            Instantiate(Enemy[Random.Range(0, Enemy.Count)], next);
            Invoke("SpawnUntilFull", 0.1f);
        }
        else
        {
            started = true;
        }
    }

    public void KillAll()
    {
        started = false;
        foreach (Transform child in transform)
        {
            foreach (Transform childChild in child)
            {
                Destroy(childChild.gameObject);
            }
        }
    }

    void Update()
    {
        transform.position += Vector3.right * (movingRight ? speed : -speed) * Time.deltaTime;

        float formationRightEdge = transform.position.x + (0.5f * widthHeight.x);
        float formationLeftEdge = transform.position.x - (0.5f * widthHeight.x);
        if (formationRightEdge > maxMove.y)
        {
            movingRight = false;
        }
        else if (formationLeftEdge < maxMove.x)
        {
            movingRight = true;
        }

        if (started)
        {
            if (Time.time > nextShot)
            {
                EnemyAll[] enemys = GetComponentsInChildren<EnemyAll>();
                int random = Random.Range(0, enemys.Length);
                int attempts = 0;
                while (!enemys[random].shouldShootBack && attempts != enemys.Length)
                {
                    random = Random.Range(0, enemys.Length);
                    attempts++;
                }

                if (attempts < enemys.Length)
                {
                    Instantiate(enemys[random].shootObject, enemys[random].transform.position, Quaternion.identity);
                }
                NextShot();
            }

            if (AllEnemyDead())
            {
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().NotiAllDead();
                started = false;
            }
        }
    }

    void NextShot()
    {
        nextShot = Time.time + Random.value * (Random.Range(2, 4));
    }

    Transform NextPosition()
    {
        foreach (Transform child in transform)
        {
            if (child.childCount <= 0)
            {
                return child;
            }
        }
        return null;
    }

    bool AllEnemyDead()
    {
        foreach (Transform child in transform)
        {
            if (child.childCount > 0)
            {
                return false;
            }
        }
        return true;
    }
}
