﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public float speed;

	private Animator anim;

	void Start () {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, speed);
		if (GetComponent<Animator> ()) {
			anim = GetComponent<Animator> ();
		}
	}

	void Update () {
		if (anim != null && anim.GetBool ("Die")) {
			Destroy (gameObject);
		}
	}
}
