﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrl : MonoBehaviour {
	GameObject player;
	Vector3 direction;
	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag("Player");
		direction = player.transform.forward;
		transform.rotation = player.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0.0f, 0.0f, 180.0f * Time.deltaTime);
		transform.position += direction * Time.deltaTime * 100.0f;
	}

}
