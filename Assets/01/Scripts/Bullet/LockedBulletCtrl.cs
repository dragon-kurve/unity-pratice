﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedBulletCtrl : MonoBehaviour {
	GameObject target;
	public GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		target = player.GetComponent<PlayerFireCtrl>().lockedTarget;
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			Vector3 dir = target.transform.position - transform.position;
			transform.rotation = Quaternion.LookRotation(dir.normalized);
			transform.position += dir.normalized * 100.0f * Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision col) {
		if (col.gameObject == target &&
			col.gameObject.name != "Boss") {
			target.SendMessage("Hit");
			player.GetComponent<PlayerFireCtrl>().isLocked = false;
			UICtrl.instance.AddScore(20);
			Destroy(gameObject);
		}
	}
}
