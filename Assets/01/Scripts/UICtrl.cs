﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICtrl : MonoBehaviour {
	public static UICtrl instance;
	public Text scoreText;
	public Text ammoText;
	public Text specialAmmoText;
	public Text lockedText;
	int score;
	int ammo;
	int specialAmmo;
	void Awake() {
		if (!instance) {
			instance = this;
		}
	}

	public void AddScore(int n) {
		score += n;
		scoreText.text = "score: " + score;
	}

	public void UpdateAmmo(int n) {
		ammo = n;
		ammoText.text = "ammo: " + ammo;
	}

	public void UpdateSpecialAmmo(int n) {
		specialAmmo = n;
		specialAmmoText.text = "special: " + specialAmmo;
	}

	public void UpdateLocked(bool b) {
		if (b) {
			lockedText.text = "locked";
		} else {
			lockedText.text = "free";
		}
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
