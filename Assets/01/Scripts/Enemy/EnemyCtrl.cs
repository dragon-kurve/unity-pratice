﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCtrl : MonoBehaviour {
	protected GameObject player;
	int state;
	float enableTime = 2.0f;
	// Use this for initialization
	protected void Start () {
		StartUp();
		float f = Random.Range(0.0f, 3.0f);
		switch((int)f) {
			case 0:
			GetComponent<Renderer>().material.color = new Color(0.0f, 0.0f, 1.0f);
			break;
			case 1:
			GetComponent<Renderer>().material.color = new Color(0.0f, 1.0f, 0.0f);
			break;
			case 2:
			GetComponent<Renderer>().material.color = new Color(1.0f, 0.4f, 0.7f);
			break;
		}
	}

	protected void StartUp() {
		player = GameObject.FindWithTag("Player");
		GetComponent<Rigidbody>().AddForce(Random.Range(-10.0f, 10.0f),
			Random.Range(-10.0f, 10.0f),
			Random.Range(-10.0f, 10.0f));
		GetComponent<Rigidbody>().AddTorque(Random.Range(-100.0f, 100.0f),
			Random.Range(-100.0f, 100.0f),
			Random.Range(-100.0f, 100.0f));
	}

	// Update is called once per frame
	protected void Update () {
		if (state == 0) {
			enableTime -= Time.deltaTime;
			if (enableTime < 0.0f) {
				state = 1;
			}
 		} else if (state == 1) {
			
		} else if (state == 2) {
			destroyEnemy();
		}
	}

	protected void FixedUpdate() {
		if (state == 1) {
			Vector3 dir = player.transform.position - transform.position;
			GetComponent<Rigidbody>().AddForce(dir * 0.2f);
		}
	}

	protected void OnCollisionEnter(Collision col) {
		if (col.collider.tag == "Bullet") {
			Destroy(col.gameObject);
			player.GetComponent<PlayerFireCtrl>().ammo += 1;
			Hit();
		}
	}

	void Hit() {
		UICtrl.instance.AddScore(50);
		state = 2;
	}

	protected void destroyEnemy() {
		Destroy(gameObject, 2.0f);
		GetComponent<MeshRenderer>().enabled = false;
		GetComponent<Collider>().enabled = false;
		GetComponent<Rigidbody>().Sleep();
		GetComponent<ParticleSystem>().Play();
	}
}
