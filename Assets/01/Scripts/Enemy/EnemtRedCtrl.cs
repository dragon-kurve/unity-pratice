﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemtRedCtrl : EnemyCtrl {

	// Use this for initialization
	new void Start () {
		base.StartUp();
		GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
	}
	
	// Update is called once per frame
	new void Update () {
		base.Update();
	}

	new void FixedUpdate() {
		base.FixedUpdate();
	}

	new void OnCollisionEnter(Collision col) {
		base.OnCollisionEnter(col);
		if (col.collider.tag == "Bullet") {
			player.GetComponent<PlayerFireCtrl>().specialAmmo += 1;
		}
	}
}
