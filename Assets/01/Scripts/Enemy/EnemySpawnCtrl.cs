﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnCtrl : MonoBehaviour {
	public GameObject player;
	public GameObject enemy;
	public GameObject enemyRed;
	Vector3 offset;
	// Use this for initialization
	void Start () {
		InvokeRepeating("spawnEnemy", 1.0f, 1.0f);
		InvokeRepeating("spawnEnemyRed", 10.0f, 10.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void spawnEnemy() {
		offset = player.transform.forward * 100.0f;
		//offset = Vector3.forward * 100.0f;
		Instantiate(enemy, player.transform.position + offset, Quaternion.identity);
	}

	void spawnEnemyRed() {
		offset = player.transform.forward * 100.0f;
		Instantiate(enemyRed, player.transform.position + offset, Quaternion.identity);
	}

}
